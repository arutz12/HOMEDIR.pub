#___________________________________________________________________________
# generic functions

# do ls right after cd
cd () {
  if [ -n $1 ]; then
    builtin cd "$@" && ls --color
  else
    builtin cd ~ && ls --color
  fi
}

# list files only
lf () { ls -lp --color=always $@ | grep -v '\/$' }

# grep process by name and list the results nicely
pg () {
    if pgrep -f $@ > /dev/null;
    then
        pgrep -f $@ | xargs ps -o user,pid,stat,rss,vsz,pcpu,args \
                               --sort -pcpu,-rss;
    fi
}

vim () {
    if [ -f "/usr/bin/nvim" ]; then
        /usr/bin/nvim "$@"
    else
        /usr/bin/vim "$@"
    fi
}

function mkvenv38 () {
    pyenv virtualenv 3.8.13 "$1"; pyenv local "$1"
}

function mkvenv39 () {
    pyenv virtualenv 3.9.13 "$1"; pyenv local "$1"
}

#function setproxy () {
#    useproxy=`kreadconfig --file kioslaverc --group Proxy\ Settings --key ProxyType`
#
#    if [ "X$useproxy" = "X0" ]; then
#        if [ "X$http_proxy" != "X" ]; then
#            unset $http_proxy
#        fi
#    else
#        http_proxy=`kreadconfig --file kioslaverc --group Proxy\ Settings --key httpProxy`
#        export http_proxy
#    fi
#}

function vtar () {
	if [[ -f "$1" ]]; then
		case "$1" in
			#*.lrz)
			#	lrztar -v "$1" ;;
			*.tar.bz2)
				tar tjvf "$1" ;;
			*.tar.gz)
				tar tzvf "$1" ;;
			*.tar.xz)
				tar tJvf "$1" ;;
			*.rar)
				unrar l "$1" ;;
			*.tar)
				tar tvf "$1" ;;
			*.tbz2)
				tar tjvf "$1" ;;
			*.tgz)
				tar tzvf "$1" ;;
			*.zip)
				unzip -l "$1" ;;
			*.7z)
				7z l "$1" ;;
			*) echo "don't know how to extract '$1'..." ;;
		esac 
	else
		echo "'$1' is not a valid file!"
	fi
}

function detar () {
	if [[ -f "$1" ]]; then
		case "$1" in
			*.lrz)
				lrztar -d "$1" ;;
			*.tar.bz2)
				tar xjvf "$1" ;;
			*.tar.gz)
				tar xzvf "$1" ;;
			*.tar.xz)
				tar Jxvf "$1" ;;
			*.bz2)
				bunzip2 "$1" ;;
			*.rar)
				unrar x "$1" ;;
			*.gz)
				gunzip "$1" ;;
			*.tar)
				tar xf "$1" ;;
			*.tbz2)
				tar xjf "$1" ;;
			*.tgz)
				tar xzf "$1" ;;
			*.zip)
				unzip "$1" ;;
			*.Z)
				uncompress "$1" ;;
			*.7z)
				7z x "$1" ;;
			*) echo "don't know how to extract '$1'..." ;;
		esac 
	else
		echo "'$1' is not a valid file!"
	fi
}

# NX_SSH completion
compdef _hosts nx_ssh
compdef _hosts ap_ssh
compdef _hosts cmd_apollo
