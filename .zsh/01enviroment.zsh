set -a
LESS="-r -MM -e -i -g"
PAGER="less -MM -g -i -e"
LESSCHARSET=latin1
BLOCKSIZE=k
#TZ=MET
#LSCOLORS=ExGxFxdxCxDxDxabagacad
#MAILDIR=~/Maildir
#MAILPATH=~/Maildir/new
EDITOR=nvim
#prompt="%m:%3c %# "
#export GREP_OPTIONS='--color=auto'
#http_proxy="http://web-proxy.cce.hp.com:8080"

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#0F8630"

# virtualenvwrapper
#VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
