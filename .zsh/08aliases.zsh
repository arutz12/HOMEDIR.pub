alias grep='grep --color=auto'
alias l='ls -lhF --color=always --group-directories-first'
alias ll='ls -alF --color=always --group-directories-first'
alias lecso-ssh-fw="~/.ssh/lecso-portforward"
alias m=less
alias p=ping
alias srr='screen -R -D'
alias sr='screen -r'
alias trace=traceroute
alias vi=vim
#alias vim=nvim
alias xcmp="xcompmgr -c -f -n -c -C -D 5 -r 4 -l 0 -t 0 -o .6 &"
#alias vimdiff='nvim -d'

alias du='du -h'
alias df='df -h'

# TMUX
alias tm='(tmux ls | grep -vq attached && tmux attach) || tmux'

alias pscpu='ps -eo pcpu,nice,stat,time,pid,cmd --sort=-pcpu,-time \
             | sed "/^ 0.0 /d"'
alias psmem='ps -eo rss,vsz,pid,cmd --sort=-rss,-vsz \
             | awk "{ if (\$1 > 10000) print }"'

alias gvi='vi -g -geom 80x40'

# facts of the day
#alias today='grep -h -d skip `date +%m/%d` /usr/share/calendar/*'

# Commands frequency counts
alias cmdfreq='cut -d " " -f 1 < $HISTFILE | sort | uniq -c | sort -nr | head -n 10'

# Pyenv
alias rmvenv="pyenv virtualenv-delete -f"
alias mkvenv="pyenv virtualenv 3.9.13"

# TLDR
alias t="tldr -t base16"
