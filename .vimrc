" Fisa-vim-config
" http://fisadev.github.io/fisa-vim-config/
" version: 8.2

" ============================================================================
" Vim-plug initialization
" Avoid modify this section, unless you are very sure of what you are doing

let vim_plug_just_installed = 0
let vim_plug_path = expand('~/.vim/autoload/plug.vim')
if !filereadable(vim_plug_path)
    echo "Installing Vim-plug..."
    echo ""
    silent !mkdir -p ~/.vim/autoload
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    let vim_plug_just_installed = 1
endif

" manually load vim-plug the first time
if vim_plug_just_installed
    :execute 'source '.fnameescape(vim_plug_path)
endif

" Obscure hacks done, you can now modify the rest of the .vimrc as you wish :)

" ============================================================================
" Active plugins
" You can disable or add new ones here:

" this needs to be here, so vim-plug knows we are declaring the plugins we
" want to use
call plug#begin('~/.vim/plugged')

" Plugins from github repos:

Plug 'fisadev/fisa-vim-colorscheme'

" Class/module browser
Plug 'majutsushi/tagbar'

" Tab list panel
Plug 'kien/tabman.vim'

" Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Autoclose
Plug 'Townk/vim-autoclose'

" Indent text object
Plug 'michaeljsmith/vim-indent-object'

" Indentation based movements
Plug 'jeetsukumaran/vim-indentwise'

"" Yank history navigation
Plug 'vim-scripts/YankRing.vim'

Plug 'altercation/vim-colors-solarized'

let g:uname = system('uname -m')
if g:uname !~ "armv7l"
    " JSON plugin
    Plug 'elzr/vim-json', { 'for': 'json' }

    " Better file browser
    Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }

    " Code commenter
    Plug 'tpope/vim-commentary'

    Plug 'python-mode/python-mode', { 'for': 'python' }

    " Better autocompletion
    Plug 'Shougo/neocomplete.vim'
    Plug 'davidhalter/jedi-vim', { 'for': 'python' }
    "Plug 'arutz74/pyfolding', { 'for': 'python' }
    "
    " Snippets manager (SnipMate), dependencies, and snippets repo
    Plug 'sirver/ultisnips', { 'for': 'python' }
    Plug 'honza/vim-snippets', { 'for': 'python' }

    " Supertab
    Plug 'ervandew/supertab'

    " Python and other languages code checker
    Plug 'scrooloose/syntastic', { 'for': ['python', 'javascript'] }

    " Search results counter
    Plug 'henrik/vim-indexed-search'
    "
    "Javascript
    "Plug 'vimlab/neojs', {'for': 'javascript'}
    Plug 'pangloss/vim-javascript', {'for': 'javascript'}
endif

" Tell vim-plug we finished declaring plugins, so it can load them
call plug#end()

" ============================================================================
" Install plugins the first time vim runs

if vim_plug_just_installed
    echo "Installing Bundles, please ignore key map error messages"
    :PlugInstall
endif

" ============================================================================
" Vim settings and mappings
" You can edit them as you wish

" my settings
set splitbelow
set splitright
set shiftround
set ignorecase
set smartcase
set mouse=a

" no vi-compatible
set nocompatible

" allow plugins by file type (required for plugins!)
filetype plugin on
filetype indent on

" tabs and spaces handling
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4

" tab length exceptions on some file types
autocmd FileType html setlocal shiftwidth=4 tabstop=4 softtabstop=4
autocmd FileType htmldjango setlocal shiftwidth=4 tabstop=4 softtabstop=4
autocmd FileType javascript setlocal shiftwidth=4 tabstop=4 softtabstop=4

" always show status bar
set ls=2

" incremental search
set incsearch
" highlighted search results
set nohlsearch
map <ESC>h <ESC>:nohl<CR>

" syntax highlight on
syntax on

" show line numbers
set nu

" tab navigation mappings
map tn :tabn<CR>
map tp :tabp<CR>
map tm :tabm 
map tt :tabnew 
map ts :tab split<CR>
map <C-S-Right> :tabn<CR>
imap <C-S-Right> <ESC>:tabn<CR>
map <C-S-Left> :tabp<CR>
imap <C-S-Left> <ESC>:tabp<CR>

" navigate windows with meta+arrows
map <M-Right> <c-w>l
map <M-Left> <c-w>h
map <M-Up> <c-w>k
map <M-Down> <c-w>j
imap <M-Right> <ESC><c-w>l
imap <M-Left> <ESC><c-w>h
imap <M-Up> <ESC><c-w>k
imap <M-Down> <ESC><c-w>j

" old autocomplete keyboard shortcut
imap <C-J> <C-X><C-O>

" Comment this line to enable autocompletion preview window
" (displays documentation related to the selected completion option)
" Disabled by default because preview makes the window flicker
set completeopt-=preview

" save as sudo
ca w!! w !sudo tee "%"

" simple recursive grep
nmap ,r :Ack 
nmap ,wr :Ack <cword><CR>

" use 256 colors when possible
" Gvim colorscheme
if (&term =~? 'mlterm\|xterm\|xterm-256\|screen-256') || has('nvim') || has("gui_running")
	let &t_Co = 256
    "colorscheme fisa
    set background=dark
    colorscheme solarized
else
    colorscheme delek
endif

" colors for gvim
if has('gui_running')
    colorscheme solarized
    set guifont=Meslo\ LG\ M\ for\ Powerline\ 10
endif

" when scrolling, keep cursor 3 lines away from screen border
set scrolloff=3

" autocompletion of files and commands behaves like shell
" (complete only the common part, list the options that match)
set wildmode=list:longest,full

" better backup, swap and undos storage
set directory=~/.vim/dirs/tmp     " directory to place swap files in
set backup                        " make backup files
set backupdir=~/.vim/dirs/backups " where to put backup files
set undofile                      " persistent undos - undo after you re-open the file
set undodir=~/.vim/dirs/undos
set viminfo+=n~/.vim/dirs/viminfo
" store yankring history file there too
let g:yankring_history_dir = '~/.vim/dirs/'

" create needed directories if they don't exist
if !isdirectory(&backupdir)
    call mkdir(&backupdir, "p")
endif
if !isdirectory(&directory)
    call mkdir(&directory, "p")
endif
if !isdirectory(&undodir)
    call mkdir(&undodir, "p")
endif

" ============================================================================
" Plugins settings and mappings
" Edit them as you wish.

" Tagbar ----------------------------- 

" toggle tagbar display
map <F4> :TagbarToggle<CR>
" autofocus on tagbar open
let g:tagbar_autofocus = 1

" NERDTree ----------------------------- 

" toggle nerdtree display
map <F3> :NERDTreeToggle<CR>
" open nerdtree with the current file selected
nmap ,t :NERDTreeFind<CR>
" don;t show these file types
let NERDTreeIgnore = ['\.pyc$', '\.pyo$']


" Tasklist ------------------------------

" show pending tasks list
map <F2> :TaskList<CR>

" CtrlP ------------------------------

" file finder mapping
let g:ctrlp_map = ',e'
" tags (symbols) in current file finder mapping
nmap ,g :CtrlPBufTag<CR>
" tags (symbols) in all files finder mapping
nmap ,G :CtrlPBufTagAll<CR>
" general code finder in all files mapping
nmap ,f :CtrlPLine<CR>
" recent files finder mapping
nmap ,m :CtrlPMRUFiles<CR>
" commands finder mapping
nmap ,c :CtrlPCmdPalette<CR>
" to be able to call CtrlP with default search text
function! CtrlPWithSearchText(search_text, ctrlp_command_end)
    execute ':CtrlP' . a:ctrlp_command_end
    call feedkeys(a:search_text)
endfunction
" same as previous mappings, but calling with current word as default text
nmap ,wg :call CtrlPWithSearchText(expand('<cword>'), 'BufTag')<CR>
nmap ,wG :call CtrlPWithSearchText(expand('<cword>'), 'BufTagAll')<CR>
nmap ,wf :call CtrlPWithSearchText(expand('<cword>'), 'Line')<CR>
nmap ,we :call CtrlPWithSearchText(expand('<cword>'), '')<CR>
nmap ,pe :call CtrlPWithSearchText(expand('<cfile>'), '')<CR>
nmap ,wm :call CtrlPWithSearchText(expand('<cword>'), 'MRUFiles')<CR>
nmap ,wc :call CtrlPWithSearchText(expand('<cword>'), 'CmdPalette')<CR>
" don't change working directory
let g:ctrlp_working_path_mode = 0
" ignore these files and folders on file finder
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.git|\.hg|\.svn|node_modules)$',
  \ 'file': '\.pyc$\|\.pyo$',
  \ }

" Flake8 ---------------------------------
let g:flake8_show_in_file = 1
let g:flake8_show_in_gutter = 1
"autocmd! BufRead,BufWritePost *.py call Flake8()
"map <F8> :call Flake8()<CR>

" JSON
let g:vim_json_syntax_conceal = (&conceallevel > 0)
let g:vim_json_syntax_concealcursor = &concealcursor
autocmd! BufRead,BufWritePost *.json set filetype=json 

augroup json_autocmd 
  autocmd! 
  autocmd FileType json set autoindent 
  autocmd FileType json set formatoptions=tcq2l 
  autocmd FileType json set textwidth=78 shiftwidth=2 
  autocmd FileType json set softtabstop=2 tabstop=8 
  autocmd FileType json set expandtab 
  autocmd FileType json set foldmethod=syntax 
augroup END

" Syntastic ------------------------------
map <F7> :SyntasticReset<CR>
map <F8> :SyntasticCheck<CR>

let g:syntastic_python_flake8_args = "--ignore=E501,E221,E241,E265"

" show list of errors and warnings on the current file
nmap <leader>e :Errors<CR>
" check also when just opened the file
let g:syntastic_check_on_open = 0
" don't put icons on the sign column (it hides the vcs status icons of signify)
let g:syntastic_enable_signs = 0
" custom icons (enable them if you use a patched font, and enable the previous 
" setting)
"let g:syntastic_error_symbol = '✗'
"let g:syntastic_warning_symbol = '⚠'
"let g:syntastic_style_error_symbol = '✗'
"let g:syntastic_style_warning_symbol = '⚠'

  " syntastic {{{
  nnoremap <silent>,l :SyntasticToggleMode<CR>
  nnoremap <silent>,L :SyntasticCheck<CR>

  " let g:syntastic_check_on_open = 0
  let g:syntastic_enable_highlighting = 1
  let g:syntastic_echo_current_error = 0

  " let g:syntastic_javascript_checkers = [ 'jshint', 'jscs' ]
  let g:syntastic_javascript_checkers = [ 'eslint' ]
  "let g:syntastic_python_checkers = [ 'pylint' ]
  let g:syntastic_python_checkers = [ 'flake8' ]
  let g:syntastic_loc_list_height=5

  " let g:syntastic_ruby_checkers = [ 'mri' ]
  " let g:syntastic_sh_checkers = [ 'sh' ]
  " let g:syntastic_slim_checkers = []

  " let g:syntastic_puppet_puppetlint_args = '--no-documentation-check'
  " let g:syntastic_ruby_rubocop_exec = expand('~/.rbenv/shims/rubocop')
  " let g:syntastic_ruby_rubocop_args = '-D'
  " let g:syntastic_ruby_slimrb_exec = expand('~/.rbenv/shims/slimrb')
  " let g:syntastic_sass_sass_exec = expand('~/.rbenv/shims/sass')
  " let g:syntastic_sass_sass_args = '-I .'
  " let g:syntastic_scss_sass_exec = expand('~/.rbenv/shims/sass')
  " let g:syntastic_scss_sass_args = '-I .'
  " let g:syntastic_sh_shellcheck_exec = 'shellcheck'
  " let g:syntastic_sh_shellcheck_args = '-e 2064,2086,2139,2155'

  " autocmd vimrc BufRead */app/views/**/*.js.coffee
  "   \ let b:syntastic_coffee_checkers = []
  " autocmd vimrc BufEnter *.coffee
  "   \ let g:syntastic_coffee_checkers = [ 'coffee', 'coffeelint' ]

  if !&diff
    let g:syntastic_auto_loc_list = 1
    let g:syntastic_auto_jump = 1
  endif
  " }}}

let g:pyfolding = 1

" Python-mode ------------------------------

" don't use linter, we use syntastic for that
let g:pymode = 1
let g:pymode_python = 'python3'
let g:pymode_virtualenv = 0
let g:pymode_run = 0
let g:pymode_indent = 1
let g:pymode_breakpoint = 0
let g:pymode_lint = 0
"let g:pymode_lint_signs = 1
"let g:pymode_lint_on_write = 1
"let g:pymode_lint_cwindow = 1
"let g:pymode_lint_message = 1
"let g:pymode_lint_checkers = ['pyflakes', 'pep8']
"let g:pymode_quickfix_minheight = 3
"let g:pymode_quickfix_maxheight = 6
"let g:pymode_lint_ignore = "E501,E221,E241,E265"
let g:pymode_syntax_all = 1
let g:pymode_motion = 1
let g:pymode_trim_whitespaces = 0
" don't fold python code on open
let g:pymode_folding = 1
"set foldcolumn = 1
" don't load rope by default. Change to 1 to use rope
let g:pymode_rope = 0
let g:pymode_rope_completion = 0
" open definitions on same window, and custom mappings for definitions and
" occurrences
"let g:pymode_rope_goto_definition_bind = ',d'
"let g:pymode_rope_goto_definition_cmd = 'e'
"nnoremap <silent> ,L :PymodeLint<CR>
"nmap ,C :tab split<CR>:PymodePython rope.goto()<CR>
"nmap ,o :RopeFindOccurrences<CR>

"inoremap <expr><TAB> pumvisible() ? "\<C-y>" : "\<CR>"

" Ultisnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsUsePythonVersion = 3

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" NeoComplete ------------------------------

let g:neocomplete#enable_at_startup =1
let g:neocomplete#enable_ignore_case = 1
let g:neocomplete#enable_smart_case = 1
let g:neocomplete#enable_auto_select = 1
let g:neocomplete#enable_fuzzy_completion = 1
let g:neocomplete#auto_completion_start_length = 1
let g:neocomplete#manual_completion_start_length = 1
let g:neocomplete#min_keyword_length = 4
let g:neocomplete#sources#syntax#min_keyword_length = 2
"let g:neocomplete#max_list = 10
let g:neocomplete#enable_auto_close_preview = 1
"let g:neocomplete#enable_auto_delimiter = 1
"let g:neocomplete#sources = '_'
let  g:neocomplete#same_filetypes = {}
let  g:neocomplete#same_filetypes._ = '_'
let g:neocomplete#data_directory='~/.cache/neocomplete'

let g:jedi#force_py_version = 3

" Supertab
let g:SuperTabDefaultCompletionType = "<c-n>"

" TabMan ------------------------------

" mappings to toggle display, and to focus on it
let g:tabman_toggle = 'tl'
let g:tabman_focus  = 'tf'

" Autoclose ------------------------------

" Fix to let ESC work as espected with Autoclose plugin
let g:AutoClosePumvisible = {"ENTER": "\<C-Y>", "ESC": "\<ESC>"}

" Window Chooser ------------------------------

" mapping
nmap  -  <Plug>(choosewin)
" show big letters
let g:choosewin_overlay_enable = 1

" Airline ------------------------------

let g:airline_powerline_fonts = 1
"let g:airline_theme = 'kalisi'
"let g:airline_theme = 'cool'
let g:airline_theme = 'solarized'
let g:airline#extensions#whitespace#enabled = 0
let g:airline_solarized_normal_green = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#tagbar#enabled = 1

" to use fancy symbols for airline, uncomment the following lines and use a
" patched font (more info on the README.rst)
"if !exists('g:airline_symbols')
"   let g:airline_symbols = {}
"endif
"let g:airline_left_sep = '⮀'
"let g:airline_left_alt_sep = '⮁'
"let g:airline_right_sep = '⮂'
"let g:airline_right_alt_sep = '⮃'
"let g:airline_symbols.branch = '⭠'
"let g:airline_symbols.readonly = '⭤'
"let g:airline_symbols.linenr = '⭡'

" ,N - toggle line numbers
nnoremap <silent> ,N :call <SID>ToggleLineNumbers()<CR>
function! s:ToggleLineNumbers() " {{{
  if &number
	set nonumber norelativenumber
  else
	set number norelativenumber
  endif
endfunction " }}}
